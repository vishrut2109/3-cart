const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],

    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

// Q1. Find all the items with price more than $65.

products.filter((item) => {
  const values = Object.entries(item);
  console.log(
    values.filter((item) => {
      if (Array.isArray(item[1])) {
        console.log(
          item[1].filter((element) => {
            return Object.entries(element)[0][1].price.replace("$", "") > 65;
          })
        );
      } else {
        return item[1].price.replace("$", "") > 65;
      }
    })
  );
});

// Q2. Find all the items where quantity ordered is more than 1.

products.filter((item) => {
  const values = Object.entries(item);
  console.log(
    values.filter((item) => {
      if (Array.isArray(item[1])) {
        console.log(
          item[1].filter((element) => {
            return Object.entries(element)[0][1].quantity > 1;
          })
        );
      } else {
        return item[1].quantity > 1;
      }
    })
  );
});

// Q.3 Get all items which are mentioned as fragile.

products.filter((item) => {
  const values = Object.entries(item);
  console.log(
    values.filter((item) => {
      if (Array.isArray(item[1])) {
        console.log(
          item[1].filter((element) => {
            return Object.entries(element)[0][1].type === "fragile";
          })
        );
      } else {
        return item[1].type === "fragile";
      }
    })
  );
});

// Q.4 Find the least and the most expensive item for a single quantity.

const output = products.reduce(
  (leastAndMostExpensive, item) => {
    const values = Object.entries(item);

    return values.filter((item) => {
      if (Array.isArray(item[1])) {
        console.log(item[1]);
        console.log(
          item[1].filter((element) => {
            if (Object.entries(element)[0][1].quantity === 1) {
              if (
                Object.entries(element)[0][1].price.replace("$", "") <
                leastAndMostExpensive.leastPrice
              ) {
                leastAndMostExpensive.leastName = Object.entries(element)[0][0];
                leastAndMostExpensive.leastPrice =
                  Object.entries(element)[0][1].price;
              }
              if (
                Object.entries(element)[0][1].price.replace("$", "") >
                leastAndMostExpensive.mostPrice
              ) {
                leastAndMostExpensive.mostName = Object.entries(element)[0][0];
                leastAndMostExpensive.mostPrice =
                  Object.entries(element)[0][1].price;
              }
              return true;
            }
          })
        );
      } else {
        if (item[1].quantity === 1) {
          if (
            item[1].price.replace("$", "") < leastAndMostExpensive.leastPrice
          ) {
            leastAndMostExpensive.leastName = item[0];
            leastAndMostExpensive.leastPrice = item[1].price;
          }
          if (
            item[1].price.replace("$", "") > leastAndMostExpensive.mostPrice
          ) {
            leastAndMostExpensive.mostName = item[0];
            leastAndMostExpensive.mostPrice = item[1].price;
          }
          return leastAndMostExpensive;
        }
      }
      console.log(leastAndMostExpensive);
    });
  },
  {
    leastName: "",
    mostName: "",
    leastPrice: Number.MAX_SAFE_INTEGER,
    mostPrice: 0,
  }
);
